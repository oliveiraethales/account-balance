# Accounts Balance CLI
The Accounts Balance CLI goal is to parse accounts and transactions CSV files and calculate the current actual balance of the provided accounts.

This README is also available in [portuguese](README-ptbr.md).

# Setup
- Install Go
- To run the main CLI, run `./account-balance accounts.csv transactions.csv` from the project dir. The main binary has been pushed to the repository to make it easier and simple to run. Another option is to run `make run acc=accounts.csv tra=transactions.csv`.
- To run the tests, install the `testify` package (`go get github.com/stretchr/testify`) and then run `go test`

