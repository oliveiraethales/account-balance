package main

func (acc *Account) ProcessTransaction(amount int) {
	acc.ActualBalance += amount

	if acc.ActualBalance < 0 {
		acc.ActualBalance -= 5
	}
}
