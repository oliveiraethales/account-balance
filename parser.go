package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

func parseAccountsCSV() {
	f, reader, err := parseCSV(accountsCSVPath)
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	for {
		data, err := reader.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Println(err)
			continue
		}

		accountID, amount, err := parseRow(data)
		if err != nil {
			log.Println(err)
			continue
		}

		_, found := accounts[accountID]
		if found {
			log.Printf("Warning: multiple entries found for account %d. Only the last one will be used.\n", accountID)
		}

		accounts[accountID] = Account{
			Id:            accountID,
			ActualBalance: amount,
		}
	}

	if len(accounts) == 0 {
		log.Fatal("No valid accounts found on CSV")
	}
}

func parseTransactionsCSV() {
	f, reader, err := parseCSV(transactionsCSVPath)
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	for {
		data, err := reader.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatal(err)
		}

		accountID, amount, err := parseRow(data)
		if err != nil {
			log.Println(err)
			continue
		}

		acc, found := accounts[accountID]
		if !found {
			log.Printf("Account ID '%d' found on transactions file but not on accounts file, skipping.\n", accountID)
			continue
		}

		acc.ProcessTransaction(amount)
		accounts[accountID] = acc
	}
}

func parseCSV(path string) (f *os.File, r *csv.Reader, err error) {
	f, err = os.Open(path)
	if err != nil {
		return nil, r, err
	}

	r = csv.NewReader(f)
	r.FieldsPerRecord = 2

	return f, r, err
}

func parseRow(data []string) (accountID int, amount int, err error) {
	if len(data) < 2 {
		return accountID, amount, fmt.Errorf("not enough elements (2 required)")
	}

	accountID, err = strconv.Atoi(data[0])
	if err != nil {
		return accountID, amount, fmt.Errorf("skipping account %s: invalid ID", data[0])
	}

	amount, err = strconv.Atoi(data[1])
	if err != nil {
		return accountID, amount, fmt.Errorf("skipping account %s: invalid amount (%s)", data[0], data[1])
	}

	return accountID, amount, nil
}
