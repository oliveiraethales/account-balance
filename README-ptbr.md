# Accounts Balance CLI - Executável para processar contas e transações
O objetivo do desta CLI goal é processar arquivos CSV de contas e transações e calcular o saldo atual das contas fornecidas.

Este README também está disponível em [inglês](README.md).

# Setup
- Instalar o Go
- Para rodar o executável principal, basta rodar `./account-balance accounts.csv transactions.csv` no diretório do projeto. O binário principal está no repositório para facilitar e simplificar a execução. Uma outra opção é rodar `make run acc=contas.csv tra=transacoes.csv`.
- Para rodar os testes, instale o package `testify` (`go get github.com/stretchr/testify`) e então rode `go test`.
