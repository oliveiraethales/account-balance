package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
)

var (
	accountsCSVPath     string
	transactionsCSVPath string
	outputPath          string
	accounts            = map[int]Account{}
)

func main() {
	flag.StringVar(&outputPath, "output", "", "optional output file")
	flag.Parse()

	if flag.NArg() < 2 {
		log.Println("Not enough arguments. Two CSV paths (accounts and transactions) are required.")
		flag.Usage()
		os.Exit(1)
	}

	accountsCSVPath = os.Args[1]
	transactionsCSVPath = os.Args[2]

	parseAccountsCSV()
	parseTransactionsCSV()

	log.Printf("Parsed %d accounts. Result:\n", len(accounts))

	for _, acc := range accounts {
		fmt.Printf("%d,%d\n", acc.Id, acc.ActualBalance)
	}

	if outputPath != "" && len(accounts) > 0 {
		f, err := os.Open(outputPath)
		if err != nil {
			log.Println("Error while saving output file: ", err)
		}

		defer f.Close()

		w := csv.NewWriter(f)

		for _, acc := range accounts {
			w.Write([]string{strconv.Itoa(acc.Id), strconv.Itoa(acc.ActualBalance)})
		}

		w.Flush()
	}
}
