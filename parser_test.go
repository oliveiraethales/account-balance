package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseAccountsCSVWithValidFile(t *testing.T) {
	accountsCSVPath = "test/accounts_valid.csv"
	parseAccountsCSV()

	expected := map[int]Account{
		345: {
			Id:            345,
			ActualBalance: 100,
		},
	}

	assert.Equal(t, expected, accounts)
}

func TestParseAccountsCSVWithMultipleAccounts(t *testing.T) {
	accountsCSVPath = "test/accounts_multiple.csv"
	parseAccountsCSV()

	expected := map[int]Account{
		345: {
			Id:            345,
			ActualBalance: 200,
		},
		100: {
			Id:            100,
			ActualBalance: 1000,
		},
	}

	assert.Equal(t, expected, accounts)
}

func TestParseAccountsCSVWithInvalidAccountId(t *testing.T) {
	accounts = map[int]Account{}
	accountsCSVPath = "test/accounts_invalid_id.csv"
	parseAccountsCSV()

	expected := map[int]Account{
		100: {
			Id:            100,
			ActualBalance: 1000,
		},
	}

	assert.Equal(t, expected, accounts)
}

func TestParseAccountsCSVWithInvalidAmount(t *testing.T) {
	accounts = map[int]Account{}
	accountsCSVPath = "test/accounts_invalid_amount.csv"
	parseAccountsCSV()

	expected := map[int]Account{
		345: {
			Id:            345,
			ActualBalance: 100,
		},
	}

	assert.Equal(t, expected, accounts)
}

func TestParseTransactionsCSVWithValidFile(t *testing.T) {
	transactionsCSVPath = "test/transactions_valid.csv"
	accounts = map[int]Account{
		345: {
			Id:            345,
			ActualBalance: 100,
		},
	}

	parseTransactionsCSV()

	expected := map[int]Account{
		345: {
			Id:            345,
			ActualBalance: 200,
		},
	}

	assert.Equal(t, expected, accounts)
}

func TestParseTransactionsCSVWithMultipleTransactions(t *testing.T) {
	transactionsCSVPath = "test/transactions_multiple.csv"
	accounts = map[int]Account{
		345: {
			Id:            345,
			ActualBalance: 100,
		},
		100: {
			Id:            100,
			ActualBalance: 200,
		},
	}

	parseTransactionsCSV()

	expected := map[int]Account{
		345: {
			Id:            345,
			ActualBalance: 400,
		},
		100: {
			Id:            100,
			ActualBalance: -805,
		},
	}

	assert.Equal(t, expected, accounts)
}

func TestParseTransactionsCSVWithInvalidAccountId(t *testing.T) {
	transactionsCSVPath = "test/transactions_invalid_id.csv"
	accounts = map[int]Account{
		100: {
			Id:            100,
			ActualBalance: 10,
		},
	}

	parseTransactionsCSV()

	expected := map[int]Account{
		100: {
			Id:            100,
			ActualBalance: 1010,
		},
	}

	assert.Equal(t, expected, accounts)
}

func TestParseTransactionsCSVWithInvalidAmount(t *testing.T) {
	transactionsCSVPath = "test/transactions_invalid_amount.csv"
	accounts = map[int]Account{
		100: {
			Id:            100,
			ActualBalance: 100,
		},
		345: {
			Id:            345,
			ActualBalance: 500,
		},
	}

	parseTransactionsCSV()

	expected := map[int]Account{
		100: {
			Id:            100,
			ActualBalance: 100,
		},
		345: {
			Id:            345,
			ActualBalance: 600,
		},
	}

	assert.Equal(t, expected, accounts)
}
