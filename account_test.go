package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestProcessTransactionWithPositiveAmount(t *testing.T) {
	acc := Account{
		ActualBalance: 100,
	}

	acc.ProcessTransaction(200)

	assert.Equal(t, 300, acc.ActualBalance)
}

func TestProcessTransactionNegativeAmount(t *testing.T) {
	acc := Account{
		ActualBalance: 100,
	}

	acc.ProcessTransaction(-200)

	assert.Equal(t, -105, acc.ActualBalance)
}
